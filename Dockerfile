FROM ubuntu:16.04

RUN apt-get update && apt-get install -y libssl-dev/xenial openssl/xenial
RUN apt-get update && apt-get install python3 python3-setuptools build-essential python3-dev python3-pip git cron --force-yes -y

# Prepare ssh deploy key to install package from gitlab.com
ARG id_rsa_path=id_rsa_assets_db
RUN mkdir /root/.ssh
COPY $id_rsa_path /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

WORKDIR /assets_retrieval
COPY . /assets_retrieval/
RUN pip3 install setuptools==40.8.0
RUN python3 setup.py install
RUN rm -rf docs test
RUN rm .gitignore AUTHORS.rst CHANGELOG.rst crontab_sample Dockerfile LICENSE.txt README.md requirements.txt settings_sample.cfg settings.cfg


# Prepare for config - has to be mounted!!
RUN mkdir /root/.dma
RUN touch /root/.dma/assets_retrieval.cfg

# Prepare for cron - has to be mounted!!
RUN touch /etc/cron.d/assets_retrieval-cron
RUN chmod 0644 /etc/cron.d/assets_retrieval-cron
RUN touch /var/log/cron.log

# Create AssetsDB
RUN mkdir db
RUN assets_db -u sqlite:///db/assets.db create_db

CMD crontab /etc/cron.d/assets_retrieval-cron && cron && tail -f /var/log/cron.log
