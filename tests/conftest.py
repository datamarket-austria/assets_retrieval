#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Dummy conftest.py for assets_retrieval.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    https://pytest.org/latest/plugins.html
"""

import pytest
import os

from assets_db.utils import create_db, create_session
from assets_db.model import Assets, Sync
from assets_retrieval.configuration import load_config


def settings_path():
    test_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(test_dir, 'data', 'dummy_settings.cfg')


def get_sqlite():
    load_config(keys=['DMA_ASSETS_DB_URL'], path=settings_path())
    return os.environ['DMA_ASSETS_DB_URL']


def get_db_path():
    load_config(keys=['DMA_ASSETS_DB_URL'], path=settings_path())
    return '.' + os.environ['DMA_ASSETS_DB_URL'][9:]


@pytest.fixture()
def test_settings():
    return settings_path()


@pytest.fixture(scope='module')
def setup_db(request):
    create_db(url=get_sqlite())

    def delete():
        os.remove(get_db_path())
    request.addfinalizer(delete)


@pytest.fixture()
def session(request):
    session = create_session(get_sqlite())

    def end_session():
        session.query(Assets).delete()
        session.query(Sync).delete()
        session.commit()
        session.close()
    request.addfinalizer(end_session)

    return session
