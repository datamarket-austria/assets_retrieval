"""
This file can only be run if you have a settings file with proper DMA credentials!
"""

from assets_retrieval.utils import refresh_token
from assets_retrieval.retrieve import get_assets_from_blockchain


def test_refresh_token():
    assert refresh_token() is not None


def test_get_assets_from_blockchain():
    assets, status = get_assets_from_blockchain(2)
    assert status
    assert len(assets) == 2


if __name__ == '__main__':
    test_refresh_token()
    test_get_assets_from_blockchain()
