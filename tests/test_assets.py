from datetime import datetime, timedelta

from assets_db.utils import MetadataAsset
from assets_retrieval.retrieve import get_random_assets, set_identifier_with_assets


def test_get_random_asset():
    l = 3
    random_asset = get_random_assets(l)
    assert len(random_asset) == l
    assert type(random_asset[0]) == str


def test_set_identifiers_with_assets(setup_db, session, test_settings):
    d = datetime(2019, 1, 1)
    external_md = [MetadataAsset('id' + str(n), d + timedelta(days=n)) for n in range(0, 3)]
    md1 = set_identifier_with_assets(external_md[0:2], from_blockchain=False, want_record_list=True,
                                     cfg_path=test_settings)
    assert len(md1) == 2
    assert type(md1[0]) == MetadataAsset

    md2 = set_identifier_with_assets(external_md[:], from_blockchain=False, want_record_list=True,
                                     cfg_path=test_settings)
    assert len(md2) == 3
    assert md1[:] == md2[1:]

# Missing test_get_assets_from_blockchain -> real credentials are needed!
