import os
from shutil import copyfile

from assets_retrieval.configuration import load_config, get_config_path


def test_get_config_path(test_settings):
    os.environ['DMA_ASSETS_RETRIEVAL_CONFIG_PATH'] = test_settings
    assert get_config_path() == test_settings

    os.environ.pop('DMA_ASSETS_RETRIEVAL_CONFIG_PATH')
    assert get_config_path(test_settings) == test_settings

    home_settings = os.path.expanduser('~/.dma/assets_retrieval.cfg')
    copy = not os.path.isfile(home_settings)
    if copy:
        copyfile(test_settings, home_settings)
    assert get_config_path() == home_settings
    if copy:
        os.remove(home_settings)


def test_load_config_not_set_keys(test_settings):
    if not os.environ.get('CAS_CLIENT_ID') is None:
        os.environ.pop('CAS_CLIENT_ID')
    if not os.environ.get('DMA_ASSETS_DB_URL') is None:
        os.environ.pop('DMA_ASSETS_DB_URL')

    load_config(keys=['CAS_CLIENT_ID', 'DMA_ASSETS_DB_URL'], path=test_settings)
    assert os.environ.get('CAS_CLIENT_ID') == 'client_id'
    assert os.environ.get('DMA_ASSETS_DB_URL') == 'sqlite:///assets.db'
    os.environ.pop('CAS_CLIENT_ID')
    os.environ.pop('DMA_ASSETS_DB_URL')


def test_load_config_set_keys(test_settings):
    if not os.environ.get('CAS_CLIENT_ID') is None:
        os.environ.pop('CAS_CLIENT_ID')
    if not os.environ.get('DMA_ASSETS_DB_URL') is None:
        os.environ.pop('DMA_ASSETS_DB_URL')

    os.environ['CAS_CLIENT_ID'] = 'client2'
    assert os.environ.get('CAS_CLIENT_ID') == 'client2'
    assert os.environ.get('DMA_ASSETS_DB_URL') is None

    load_config(keys=['CAS_CLIENT_ID', 'DMA_ASSETS_DB_URL'], path=test_settings)
    assert os.environ.get('CAS_CLIENT_ID') == 'client2'
    assert os.environ.get('DMA_ASSETS_DB_URL') == 'sqlite:///assets.db'
