from owslib.csw import CatalogueServiceWeb
from owslib.fes import PropertyIsEqualTo
from datetime import datetime, date

from assets_retrieval.csw import get_external_md_list, get_md
from assets_db.utils import MetadataAsset


def test_get_external_md_list():
    test_id = 'S1A_EW_GRDM_1SSH_20170101T220602_20170101T220639_014644_017D07_DF5D'
    constraint = [PropertyIsEqualTo('apiso:Identifier', test_id)]
    csw = CatalogueServiceWeb('https://csw.eodc.eu')

    md_assets = get_external_md_list(csw, constraint)
    assert len(md_assets) == 1
    assert type(md_assets[0]) == MetadataAsset
    assert md_assets[0].identifier == test_id
    assert md_assets[0].dateModified == datetime(2019, 2, 11)


def test_get_md_identifiers(test_settings):
    test_id = 'S1A_EW_GRDM_1SSH_20170101T220602_20170101T220639_014644_017D07_DF5D'
    md_assets = get_md(identifiers=[test_id], cfg_path=test_settings)
    assert len(md_assets) == 1
    assert type(md_assets[0]) == MetadataAsset
    assert md_assets[0].identifier == test_id
    assert md_assets[0].dateModified == datetime(2019, 2, 11)


def test_get_md(test_settings):
    parent_id = 's1a_csar_grdh_ew'
    d = date(2019, 2, 1)
    md_assets = get_md(parentidentifier=parent_id, start_date=d, end_date=d, cfg_path=test_settings)
    assert len(md_assets) == 4
    assert type(md_assets[0]) == MetadataAsset


def test_get_md_series(test_settings):
    md_assets = get_md(series_only=True, cfg_path=test_settings)
    assert len(md_assets) == 14


if __name__ == '__main__':
    pass
