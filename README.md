# assets_retrieval

Get DMA Asset-IDs or directly create an offer for local identifiers. Uses [assetsDB]
database to store Asset-IDs and Dataset PUIDs.

## Description

This package can be used as template how a matching between external node's identifiers and DMA Asset-IDs can be matched
and stored. It gets external node identifiers and one asset each and stores those pairs in a
[assetsDB] Additionally it is possible to directly create an offer in DMA.
To use this package for an other external node (currently working
for eodc's CSW server) only small modifications have to be made (see section *Changes for different external nodes*).


## Installation

### Configuration
A simple config file is used to configure the package. ``settings_sample.cfg`` shows the accepted structure and parameters.
In the following the single parameters are described in more detail:

- [blockchain]
    - host: blockchain url where request should be sent to (e.g. to request assets or create an offer,
    https://smart-contract-ui.datamarket.at)
    - default_licence: Choose or create a licence in [Agreed] and add the hash here.
    When a offer is created and no licence is specified this default licence is used.
- [assets_db]
    - url: database url to AssetsDB. (Location of sqlite file)
- [cas]  
    Client and user need to be registered. See [Authentication]
    for more information.
    - client_id = client name
    - client_secret = client password
    - user_id = username
    - user_secret = user password
    - token_url = url to get access token from (https://cas.datamarket.at/cas/oauth2.0/accessToken)
- [external_node_endpoint]  
    Needs to match host and port of external_node_endpoint. The external_node_endpoint service has to be up and running to
    create an offer!
    (external_node_endpoint's hostname in docker-compose)
    - service_host = host where external_node_endpoint service is running. For a locally deployed external_node_endpoint
    host should be 0.0.0.0 or if you are using the provided docker-compose (see *Using docker-compose* section) file set
    the host to external_node_endpoint
    - service_port = port where external_node_endpoint service is running (e.g. 5000)
- [csw]  
    If you have a CSW server to provide metadata add the csw url here, otherwise delete this section and
    add additional parameters needed to access your metadata. Keep in mind to also change configuration.py for need
    parameters.
    - url: url to CSW server


### Virtualenv
This section explains how this package can be installed into a virtual environment
1. Clone git repository (using ssh in this case) and navigate into folder
    ```bash
    git clone git@gitlab.com:datamarket/assets_retrieval.git
    cd /path/to/assets_retrieval
    ```
1. Create/Modify settings file
    ```bash
    mkdir ~/.dma
    cp settings_sample.cfg ~/.dma/assete_retrieval.cfg
    ```
    Inside the new assete_retrieval.cfg file add needed parameters (see *Configuration* section above)
1. Recommended: Create a virtual environment and activate it, tested with Python 3.6
    ```bash
    virtualenv assets_db -p python3.6
    source assets_retrieval/bin/activate
    ```
1. Install package (Make sure you are inside the assets_retrieval folder)
    ```bash
    cd /path/to/assets_retrieval
    python setup.py install
    ```
    Replace ``install`` with ``develop`` if you plan to modify code.


### Docker
In this package also a Dockerfile and crontab file are included to simplify the setup and usage.
Depending where, how often and which command should run modify the crontab file (Take a look at *Usage* section to see
possible commands).

Example:
- Runs every day at midnight and adds Asset-IDs for all identifiers modified the day before.
    ```bash
     0 0 * * * /usr/local/bin/assets_retrieval -v -s $(date --date="yesterday" +"\%Y-\%m-\%d") -e $(date --date="yesterday" +"\%Y-\%m-\%d") add_assets >> /var/log/cron.log 2>&1
    ```
- Add Asset-IDs for products only
    ```bash
     0 0 * * * /usr/local/bin/assets_retrieval -v -o add_assets >> /var/log/cron.log 2>&1
    ```
- Add completely synced date
    ```bash
    0 0 * * * /usr/local/bin/assets_db -v -c /root/.dma/assets_retrieval.cfg insert_sync $(date --date="yesterday" +"\%Y-\%m-\%d")
    ```

#### Manual setup:

1. Create assetsDB volume:
    ```bash
    docker volume create assets-db-data
    ```

1. Build docker image:
    ```bash
    docker build -t dma/assets_retrieval .
    ```
    Note: A ssh deploy key is used to install the [assetsDB] package. The argument
   ``id_rsa_path`` can be set to pass the path to this deploy key (add ``--build-arg id_rsa_path=/path/to/id_rsa`` to
    build command). If not set the deploy key is assumed to be in the same folder as the Dockerfile.

1. Prepare config and crontab:
    Copy the files:
    ```bash
    cp settings_sample.cfg assets_retrieval.cfg
    cp crontab_sample assets_retrieval-crontab
    ```
    Open the copied files and set parameters/command (see *Configuration* section and example above). These files will
    be mounted into the container in the next step, therefore the image does not need to be rebuild if the config or
    crontab changes.

1. Run container:
    ```bash
    docker run --name assets_retrieval \
    -v assets-db-db:/assets_retrieval/db \
    -v ./assets_retrieval.cfg:/root/.dma/assets_retrieval.cfg \
    -v ./assets_retrieval-crontab:/etc/cron.d/assets_retrieval-cron \
    dma/assets_retrieval
    ```
    The cron job will run automatically inside the container e.g. filling AssetsDB every night with Asset-IDs.

#### Using docker-compose
Take a look at [externalNodeSetup] for an example of a docker-compose
file which deploys all needed components to become a dynamic data provider.


## Usage

When the package is installed a console script is added:

``assets_retrieve``: This is the wrapper for all processes that can be run. It accept either a list of identifiers or
start, end date, product name and products_only as parameters. Please use ``--help`` to get a complete overview of
possible parameters and their format.

Currently there are two subcommands that can be called with ``assets_retrieve``:

- ``add_assets``: request assets from the blockchain and store those in the AssetsDB
- ``create_offer``: create an offer in the blockchain, optionally a licence can be added

Make sure to always put the subcommand after the parameters of ``assets_retrieve``!


## Changes for different external nodes

If this package should be used with an other metadata provider one function has to be changed.

Inside parser.py in main() get_md() is called. This function has to be reimplemented. Depending on the input (could be a
time range or a product type) the function should return a list of MetadataAsset objects (Defined in AssetDB, see current
implementation for an example). Maybe also the parsed arguments need to be changed. Keep in mind that if you change the
commandline interface to also adopted the cronjob.

If additional config settings are needed just add them to your config file and inside the configuration.py (dict).
When you want to load them used the load_config() function and specify the keys.


## Note

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.

[assetsDB]: https://gitlab.com/datamarket-austria/assets_db
[externalNodeSetup]: https://gitlab.com/datamarket-austria/external_node_setup
[Authentication]: https://datamarket.gitlab.io/Documentation/authentication/
[Agreed]: https://agreed.datamarket.at/
