import os
import requests
import logging
from datetime import datetime, timedelta

from assets_retrieval.configuration import load_config

_logger = logging.getLogger(__name__)


def token_is_expired() -> bool:
    """
    Check whether the current token is expired
    :return: [bool] whether token is expired
    """
    token_issue_time = datetime.strptime(os.environ['CAS_TOKEN_ISSUE_TIME'], '%Y-%m-%d %H:%M:%S.%f')
    token_expires_in = timedelta(seconds=int(os.environ['CAS_TOKEN_EXPIRES_IN']))
    return not datetime.now() > token_issue_time - token_expires_in


def refresh_token():
    """
    Get a token using Resource Owner Password Credentials Grant Type flow.
    Information is stored as environment variables
    :return: access token
    """
    load_config(keys=['DMA_BLOCKCHAIN_HOST', 'CAS_TOKEN_URL', 'CAS_CLIENT_ID', 'CAS_USER_ID', 'CAS_USER_SECRET'])

    # Get first token
    if os.environ.get('token') is None:
        data = {
            'client_id': os.environ['CAS_CLIENT_ID'],
            'username': os.environ['CAS_USER_ID'],
            'password': os.environ['CAS_USER_SECRET'],
            'grant_type': 'password'
        }
        cur_token = requests.post(os.environ['CAS_TOKEN_URL'], data=data).json()
        os.environ['CAS_REFRESH_TOKEN'] = cur_token['refresh_token']

    # refresh token
    elif token_is_expired():
        data = {
            'client_id': os.environ['CAS_CLIENT_ID'],
            'client_secret': os.environ['CAS_CLIENT_SECRET'],
            'grant_type': 'refresh_token',
            'refresh_token': os.environ['CAS_REFRESH_TOKEN']
        }
        cur_token = requests.post(os.environ['CAS_TOKEN_URL'], data=data).json()

    # return current token
    else:
        return os.environ['CAS_ACCESS_TOKEN']

    os.environ['CAS_ACCESS_TOKEN'] = cur_token['access_token']
    os.environ['CAS_TOKEN_EXPIRES_IN'] = str(cur_token['expires_in'])
    os.environ['CAS_TOKEN_ISSUE_TIME'] = str(datetime.now())
    return os.environ['CAS_ACCESS_TOKEN']


class AssetRetrievalException(Exception):
    def __init__(self, status_code: int, message: str):
        super().__init__(message)
        self.statusCode = status_code
        self.message = message
        self.error = ''

    def __str__(self):
        return '%s, failed with status code %d\n%s'\
               % (self.error, self.statusCode, self.message)


class AssetIDRetrievalException(AssetRetrievalException):
    def __init__(self, status_code: int, message: str):
        super().__init__(status_code, message)
        self.error = 'Asset-IDs could not be retrieved from blockchain'


class AssetStateRetrievalException(AssetRetrievalException):
    def __init__(self,  status_code: int, message: str):
        super().__init__(status_code, message)
        self.error = 'Asset State could not be retrieved from externalNodeEndpoint'


class OfferCreationException(AssetRetrievalException):
    def __init__(self,  status_code: int, message: str, identifier):
        super().__init__(status_code, message)
        self.error = 'Could not be created offer for %s' % identifier


if __name__ == '__main__':
    pass
