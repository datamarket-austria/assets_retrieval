import os
import logging
import requests
import uuid
import json
import hashlib

from assets_db.query import get_asset, get_missing_identifiers
from assets_db.insert import insert_metadata_asset
from assets_retrieval.configuration import load_config
from assets_retrieval.utils import refresh_token, AssetIDRetrievalException, AssetStateRetrievalException,\
    OfferCreationException

_logger = logging.getLogger(__name__)


def create_offers(external_md: list, licence: str=None):
    """
    Create offers for a list of identifiers with a given licence.
    If no licence is supplied the default licence is used.
    :param external_md: [list] list of MetadataAsset objects
    :param licence: [str] licence hash (has to exist in agreed!)
    """
    # make sure each identifier has an asset_id in the DB
    records = set_identifier_with_assets(external_md, from_blockchain=True, want_record_list=True)

    # create offer
    _logger.info('Start creating offers for %d identifiers ...' % len(external_md))
    for record in records:
        create_single_offer(identifier=record['identifier'], asset_id=record['asset_id'], licence=licence)
    _logger.info('Finished creating offers.')


def add_asset_ids(external_md: list, from_blockchain: bool=True):
    """
    Add assets ids to AssetsDB for list of identifiers.
    :param external_md: [list] list of MetadataAsset objects
    :param from_blockchain: [bool] whether Asset-IDs should be retrieved from blockchain (True, default) or generated randomly
    """
    _logger.info('Check/Add Asset-IDs for %d identifiers' % len(external_md))
    set_identifier_with_assets(external_md, from_blockchain=from_blockchain, want_record_list=False)
    _logger.info('Finished adding Asset-IDs ')


def create_single_offer(identifier: str, asset_id: str, licence: str=None):
    """
    Create an offer for the given identifier.
    :param identifier: [str] identifier for which the offer should be created
    :param asset_id: [str] matching asset if for identifier
    :param licence: [str] DMA licence (has to be created first at https://agreed.datamarket.at/agreed/contracts/)
    :return:
    """
    load_config(keys=['DMA_BLOCKCHAIN_HOST', 'DMA_OFFER_LICENCE_DEFAULT'])
    if licence is None:
        licence = os.environ['DMA_OFFER_LICENCE_DEFAULT']

    asset_state = get_asset_state(identifier)

    response = None
    offer_counter = 1
    token = refresh_token()
    header = {'Authorization': 'bearer ' + token}
    params = (
        ('assetState', asset_state),
        ('contractHashes', licence),
        ('autoAccept', 'true'),
        ('terminationMode', '4')
    )
    url = '%s/api/data/%s/create_offer' % (os.environ['DMA_BLOCKCHAIN_HOST'], asset_id)

    while offer_counter <= 3:
        response = requests.post(url, headers=header, params=params)
        if response.status_code == 200:
            return
        offer_counter += 1
    raise OfferCreationException(status_code=response.status_code, message=response.text, identifier=identifier)


def get_asset_state(identifier: str, counter: int=1):
    """
    Get the asset_state for an identifier.
    Creates DMA metadata xml and adds asset_id
    :param identifier: [str] identifier to get and asset_state for
    :param counter: [int] if retrieval fails counter is increased and process is run again
    :return: [str, bool] asset_state hash, status
    """
    load_config(keys=['DMA_EXTERNAL_NODE_HOST', 'DMA_EXTERNAL_NODE_PORT'])
    url = 'http://%s:%s/changelist/resource/%s' \
          % (os.environ['DMA_EXTERNAL_NODE_HOST'], os.environ['DMA_EXTERNAL_NODE_PORT'], identifier)
    response = requests.get(url)

    if response.status_code == 200:
        metadata = json.loads(response.text)
        md_binary = json.dumps(metadata).encode()
        asset_state = hashlib.sha256(md_binary).hexdigest()
        return asset_state
    elif counter <=3:
        _logger.warning('Unable to retrieve AssetState frm externalNodeEndpoint. Trying again ...')
        get_asset_state(identifier=identifier, counter=counter+1)
    else:
        raise AssetStateRetrievalException(status_code=response.status_code, message=response.text)


def set_identifier_with_assets(external_md: list, from_blockchain: bool=True, want_record_list: bool=True,
                               cfg_path: str=None) -> list:
    """
    Get dict of asset_ids, dataset_puids and identifiers and insert it into assetsDB.
    :param external_md: [list] list of MetadataAsset objects
    :param from_blockchain: [bool] whether Asset-IDs should be retrieved from blockchain (True, default) or generated
    randomly
    :param want_record_list: [bool] whether a list of record dicts should be returned (default: True)
    :param cfg_path: [str] path to config file (optional)
    :return: [list] list of record dicts
    """

    load_config(keys=['DMA_ASSETS_DB_URL'], path=cfg_path)
    missing_md = []
    existing_md = []

    # Missing identifiers
    identifiers = [e.identifier for e in external_md]
    missing = get_missing_identifiers(identifiers, blockchain_only=from_blockchain,
                                      url=os.environ['DMA_ASSETS_DB_URL'])
    num_missing = len(missing)
    asset_ids = None
    msg = '%d identifiers found with no Asset-ID' % num_missing
    msg += ' (from blockchain)' if from_blockchain else ' (random)'
    _logger.info(msg)

    if num_missing > 0:
        if from_blockchain:
            asset_ids = get_assets_from_blockchain(number=num_missing)
        else:
            asset_ids = get_random_assets(number=num_missing)

    if num_missing > 0 or want_record_list:
        while len(external_md) > 0:
            e = external_md.pop()
            if e.identifier in missing.keys():
                e.set_asset(asset_id=asset_ids.pop(), from_blockchain=from_blockchain)
                e.changeType = missing[e.identifier]
                missing_md.append(e)
            elif want_record_list:
                dataset_puid, asset_id, cur_from_blockchain = get_asset(identifier=e.identifier,
                                                                        blockchain_only=from_blockchain,
                                                                        url=os.environ['DMA_ASSETS_DB_URL'])
                e.set_asset(asset_id=asset_id, from_blockchain=from_blockchain, dataset_puid=dataset_puid)
                existing_md.append(e)
        insert_metadata_asset(missing_md, url=os.environ['DMA_ASSETS_DB_URL'])

    if want_record_list:
        missing_md.extend(existing_md)
        return missing_md


def get_assets_from_blockchain(number: int=1, counter: int=1) -> list:
    """
    Get a list of asset_ids from blockchain.
    :param number: [int] number of blockchain Asset-IDs to retrieve (default 1)
    :param counter: [int] if retrieval fails counter is increased and process is run again
    :return: [list] list of assets
    """
    # TODO max 100 assets at once!!
    load_config(keys=['DMA_BLOCKCHAIN_HOST'])

    # Get Token
    token = refresh_token()

    # Get Assets
    assets_url = '%s/api/get_asset_addresses' % (os.environ['DMA_BLOCKCHAIN_HOST'])
    headers = {'Authorization': 'bearer ' + token}
    params = (
        ('amount', number),
    )
    response = requests.get(assets_url, headers=headers, params=params)

    if response.status_code == 200:
        addresses = json.loads(response.text)
        _logger.info('%d blockchain Asset-IDs retrieved' % number)
        return addresses['addresses']
    elif counter <= 3:
        _logger.warning("Unable to get new blockchain Asset-IDs. Trying again ...")
        get_assets_from_blockchain(number=number, counter=counter+1)
    else:
        raise AssetIDRetrievalException(status_code=response.status_code, message=response.text)


def get_random_assets(number: int=1) -> list:
    """
    Get list of random asset_ids.
    Equivalent output to get_assets_from_blockchain
    :param number: [int] number of random Asset-IDs to retrun (default 1)
    :return:
    """
    random_asset_ids = [str(uuid.uuid4()) for _ in range(number)]
    _logger.info('%d random Asset-IDs retrieved' % number)
    return random_asset_ids


if __name__ == '__main__':
    pass
