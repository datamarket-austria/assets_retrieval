import os
import configparser


def load_config(keys: list=None, path: str=None):
    """
    Load environment variables form config file if not set.
    :param keys: [list] env variables to load (optional, default: all are loaded)
    :param path: [str] path to config file (optional, if not set central locations are checked)
    """
    mapping = {
        # CAS
        'CAS_CLIENT_ID': ['cas', 'client_id'],
        'CAS_CLIENT_SECRET': ['cas', 'client_secret'],
        'CAS_TOKEN_URL': ['cas', 'token_url'],
        'CAS_USER_ID': ['cas', 'user_id'],
        'CAS_USER_SECRET': ['cas', 'user_secret'],

        # asset db
        'DMA_ASSETS_DB_URL': ['assets_db', 'url'],

        # blockchain
        'DMA_BLOCKCHAIN_HOST': ['blockchain', 'host'],
        'DMA_OFFER_LICENCE_DEFAULT': ['blockchain', 'default_licence'],

        # csw
        'CSW_URL': ['csw', 'url'],

        # external node
        'DMA_EXTERNAL_NODE_HOST': ['external_node_endpoint', 'service_host'],
        'DMA_EXTERNAL_NODE_PORT': ['external_node_endpoint', 'service_port']
    }

    if keys is None:
        keys = mapping.keys()

    # Only load not set keys
    missing_keys = []
    for key in keys:
        if os.environ.get(key) is None:
            missing_keys.append(key)
    if len(missing_keys) == 0:
        return

    path = get_config_path(path)
    config = configparser.ConfigParser()
    config.read(path)

    for key in missing_keys:
        val = mapping[key]
        os.environ[key] = config.get(val[0], val[1])


def get_config_path(path: str=None):
    """
    Returns config file path
    :param path: [str] path to config file (optional, if not set central locations are checked)
    :return: [str] config file path or None
    """
    if path is not None and os.path.isfile(path):
        return path
    if os.environ.get('DMA_ASSETS_RETRIEVAL_CONFIG_PATH') is not None \
            and os.path.isfile(os.environ['DMA_ASSETS_RETRIEVAL_CONFIG_PATH']):
        return os.environ['DMA_ASSETS_RETRIEVAL_CONFIG_PATH']

    folder = 'dma'
    config_filename = 'assets_retrieval.cfg'
    etc_cfg = '/etc/%s/%s' % (folder, config_filename)
    home_cfg = os.path.expanduser('~/.%s/%s' % (folder, config_filename))
    if os.path.isfile(home_cfg):
        return home_cfg
    if os.path.isfile(etc_cfg):
        return etc_cfg
