import os
import logging
from datetime import date, datetime
from owslib.csw import CatalogueServiceWeb
from owslib.fes import PropertyIsEqualTo, PropertyIsGreaterThanOrEqualTo, PropertyIsLessThanOrEqualTo

from assets_retrieval.configuration import load_config
from assets_db.utils import MetadataAsset

_logger = logging.getLogger(__name__)


def build_constraints(parentidentifier: str=None, start_date: date=None, end_date: date=None):
    """
    Build list of csw constraints from given parameters.
    :param parentidentifier: [str] csw parentidentifier to search for (optional)
    :param start_date: [date] earliest modification date (optional)
    :param end_date: [date] latest modification date (optional)
    :return: list of constraints
    """
    constraints = []
    date_fmt = '%Y-%m-%d'

    if parentidentifier is not None:
        constraints.append(PropertyIsEqualTo('apiso:ParentIdentifier', parentidentifier))
    if start_date is not None:
        constraints.append(PropertyIsGreaterThanOrEqualTo('apiso:Modified', start_date.strftime(date_fmt)))
    if end_date is not None:
        constraints.append(PropertyIsLessThanOrEqualTo('apiso:Modified', end_date.strftime(date_fmt)))

    if len(constraints) > 1:
        constraints = [constraints]
    return constraints


def get_md(identifiers: list=None, parentidentifier: str=None, start_date: date=None, end_date: date=None,
           series_only: bool=False, cfg_path: str=None) -> list:
    """
    Get list of identifiers from constraints.
    :param identifiers: [list] list of identifiers to search for, if this argument is set all others are ignored (optional)
    :param parentidentifier: [str] csw parentidentifier to search for (optional)
    :param start_date: [list] earliest modification date (optional)
    :param end_date: [list] latest modification date (optional)
    :param series_only: [bool] whether all complete copernicus product and no single records should be taken into account (default: False)
    :param cfg_path: [str] path to config file (optional)
    :return: [list] list of identifiers matching the given constraints
    """
    load_config(keys=['CSW_URL'], path=cfg_path)
    csw = CatalogueServiceWeb(url=os.environ['CSW_URL'])

    if identifiers is not None:
        _logger.info('Using provided identifier list')
        constraints = [PropertyIsEqualTo('apiso:Identifier', i) for i in identifiers]
        return get_external_md_list(csw, constraints)
    if series_only:
        _logger.info('Using only copernicus series to create identifier list')
        constraints = [PropertyIsEqualTo('apiso:Identifier', p) for p in get_products_list()]
        return get_external_md_list(csw, constraints)

    _logger.info('Using parentidentifier/start_date/end_date to create identifier list')
    # Iterate over copernicus products and check for each product separately
    # a && b && (c || d) is not possible -> a && b && c || a && b && d
    copernicus_only = True  # whether only copernicus records should be taken into account, need to be changed in code!
    if parentidentifier is None and copernicus_only:
        identifiers_all = []
        num_of_records = 0
        products = get_products_list()
        for product in products:
            constraints = build_constraints(parentidentifier=product, start_date=start_date, end_date=end_date)
            cur_identifiers = get_external_md_list(csw=csw, constraints=constraints)
            if len(cur_identifiers) > 0:
                identifiers_all.extend(cur_identifiers)
                num_of_records += len(cur_identifiers)
        return identifiers_all

    constraints = build_constraints(parentidentifier=parentidentifier, start_date=start_date, end_date=end_date)
    return get_external_md_list(csw=csw, constraints=constraints)


def get_external_md_list(csw, constraints: list):
    """
    Return list of identifiers matching given constraints.
    :param csw: owslib.CatalogueServiceWeb
    :param constraints: [list] list of OWSLib constraints
    :return: [list] list of matching identifiers
    """
    next_record = 1
    md_assets_list = []
    max_records = 1000
    done_records = 0
    while next_record > 0:
        csw.getrecords2(constraints=constraints, maxrecords=max_records, startposition=next_record,
                        outputschema='http://www.opengis.net/cat/csw/2.0.2')
        for rec_id in csw.records:
            rec = csw.records[rec_id]
            # TODO can I get the change_type here?
            md_assets_list.append(MetadataAsset(rec.identifier, datetime.strptime(rec.modified, '%Y-%m-%d')))

        done_records += csw.results["returned"]
        _logger.debug('%d of %d records are done' % (done_records, csw.results['matches']))
        next_record = csw.results['nextrecord']
    return md_assets_list


def get_products_list() -> list:
    """
    Returns the list of products (copernicus)
    """

    return [
        's1a_csar_grdh_ew',
        's1a_csar_grdh_iw',
        's1a_csar_grdm_ew',
        's1a_csar_slc__ew',
        's1a_csar_slc__iw',
        's1b_csar_grdh_ew',
        's1b_csar_grdh_iw',
        's1b_csar_grdm_ew',
        's1b_csar_slc__iw',
        's2a_prd_msil1c',
        's2b_prd_msil1c',
        's3a_ol_1_efr',
        's3a_ol_1_err',
        's3a_sl_1_rbt'
    ]


if __name__ == '__main__':
    pass
