import sys
import argparse
import logging
from datetime import datetime

from assets_retrieval.retrieve import create_offers, add_asset_ids
from assets_retrieval.csw import get_md

_logger = logging.getLogger(__name__)


def parse_args(args):
    """
    Parse commandline parameters.
    :param args: command line parameters as list of strings
    :return: [obj:`argparse.Namespace`] command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description='Script to handle assets database an create offers in blockchain.'
    )
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO
    )
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG
    )
    parser.add_argument(
        '-i',
        '--identifiers',
        dest='identifiers',
        type=str,
        help='Comma separated list of identifiers, if this param is set all others are ignored. Example: id1,id2'
    )
    parser.add_argument(
        '-p',
        '--product',
        dest='product',
        type=str,
        help='Product type (used to create list of identifiers)'
    )
    parser.add_argument(
        '-s',
        '--start_date',
        dest='start',
        type=str,
        help='Earliest modification date e.g.: 2019-01-01 (used to create list of identifiers)'
    )
    parser.add_argument(
        '-e',
        '--end_date',
        dest='end',
        type=str,
        help='Latest modification date e.g.: 2019-01-01 (used to create list of identifiers)'
    )
    parser.add_argument(
        '-o',
        '--products_only',
        action='store_true',
        dest='products_only',
        help='Whether only product classes identifiers are used, no single records (used to create list of identifiers)'
    )

    subparser = parser.add_subparsers(
        dest='subparser_name',
        description="Commands to use with assets_retrieval"
    )

    offer_parser = subparser.add_parser(
        'create_offer',
        help='Script to create an offer'
    )
    offer_parser.add_argument(
        '-l',
        '--licence',
        type=str,
        dest='licence',
        help='Licence hash from Agreed, if not set default licence is used'
    )

    add_assets_parser = subparser.add_parser(
        'add_assets',
        help='Script to add asset_ids to to assetsDB'
    )
    add_assets_parser.add_argument(
        '-r',
        '--random_assets',
        action='store_true',
        dest='random_assets',
        help='If asset IDs should be generated randomly (not retrieved from blockchain), with this Asset IDs no offer'
             ' can be created! Should only be used for free Assets.'
    )
    return parser.parse_args(args)


def setup_logging(loglevel):
    """
    Setup basic logging
    :param loglevel: [int] minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel,
        stream=sys.stdout,
        format=logformat,
        datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """
    Main entry point for external calls.
    Set loglevel and create list of identifiers from input parameters and call method depending on subparser
    :param args: [list of str] command line parameter list
    :returns list of identifiers
    """
    args = parse_args(args)
    setup_logging(args.loglevel)

    if args.identifiers is None:
        if args.start is not None:
            args.start = datetime.strptime(args.start, '%Y-%m-%d')
        if args.end is not None:
            args.end = datetime.strptime(args.end, '%Y-%m-%d')
        external_md = get_md(parentidentifier=args.product, start_date=args.start, end_date=args.end,
                             series_only=args.products_only)
    else:
        external_md = get_md(identifiers=args.identifiers.split(','))
    _logger.debug('list of identifiers: ' + str([e.identifier for e in external_md]))

    if args.subparser_name == 'create_offer':
        create_offers(external_md=external_md, licence=args.licence)
    elif args.subparser_name == 'add_assets':
        add_asset_ids(external_md, from_blockchain=not args.random_assets)


def run():
    """Entry point for console_scripts"""
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
